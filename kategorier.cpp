/**
 * Funksjonene i klassen 'kategorier' og deres definisjoner.
 * @file kategorier.cpp
 * @author Abdulsamad Sheikh, Jamal Hussein & Ole Bjørn Halvorsen.
 */
#include <iostream>                //  cout, cin
#include <fstream>                 //  ifstream, ofstream
#include <string>
#include <map>
#include <list>
#include <algorithm>
#include "kategorier.h"
#include "LesData3.h"
#include "kunder.h"

using namespace std;

extern Kategorier gKategoribase;
extern Kunder gKundebase;

// Konstruktør
Kategorier::Kategorier() {
    sisteNr = 0;
}

// Destruktør
Kategorier::~Kategorier() {
   
}

/**
 * @param k - Kommando
 * Lar bruker gjøre et valg om hva de ønsker å gjøre med en kategori
 * @see nyKategori()
 * @see skrivUtKategorier()
 * @see skrivEnKategori()
 * @see sjekkKategori()
 * @see kategori::lagNyTing()
 * @see kategori::endreTing()
 * @see kunder::getSisteNr()
 * @see kunder::finnKunde()
 * @see kategori::skrivData()
 * @see kategori::kjopEnTing()
*/
void Kategorier::handling(char k) {
    char nyvalg = '\0';
    string navn;
    Kategori* kategori = nullptr;

    if (k != 'K') {
        nyvalg = lesChar("");
    }

    if (k == 'A') {
        switch (nyvalg) {
            case 'N':
                nyKategori();
                break;
            case 'A':
                skrivUtKategorier();
                break;
            case 'S':
                cout << "Kategorinavn: ";
                getline(cin, navn);
                skrivEnKategori(navn);
                break;
        }
    } else if (k == 'T') {
        cout << "Kategori:";
        getline(cin, navn);
        string temp = sjekkKategori(navn);

        for (const auto &val : gKategori) {
            if (temp == val.first) {
                kategori = val.second;
                break;
            }
        }

        if (kategori) {
            if (nyvalg == 'N') {
                kategori->lagNyTing();
            } else if (nyvalg == 'E') {
                kategori->endreTing();
            }
        }
    } else if (k == 'K') {
        int kundenr = lesInt("Kundenummer kjoper ", 1, gKundebase.getSisteNr());

        if (gKundebase.finnKunde(kundenr, Ingen)) {
            cout << "Kategori: ";
            getline(cin, navn);
            string temp = sjekkKategori(navn);

            for (const auto &val : gKategori) {
                if (temp == val.first) {
                    kategori = val.second;
                    break;
                }
            }

            if (kategori) {
                kategori->skrivData();
                int tingNr = lesInt("Hvilken ting ønsker du å kjøpe? ", 0, getTingNr());

                if (tingNr > 0 && !kategori->sjekkKundeNummer(kundenr, tingNr)) {
                    kategori->kjopEnTing(kundenr, tingNr);
                } else {
                    cout << (tingNr > 0 ? "NB: Man kan ikke kjope av seg selv " : "Ingen ting ble kjøpt");
                }
            }
        }
    }
}

/**
* Siste nr om ting som ble brukt returneres.
*
* @return sisteNr - siste nr på tingen returneres.
*/
int Kategorier::getTingNr() const{
    return sisteNr;
}

/**
* Funkjsonen øker siste nummeret med en.
*/
void Kategorier::leggTilSistenr(){
  sisteNr++;
}


/**
 * Alt som ligger i filen 'KATEGORIER.DTA' blir lest inn.
 * Dette innebærer alle kategorier og deres datatmedlemmer
*/
void Kategorier::lesFraFil() {
    ifstream innfil;
        innfil.open
    ("/Users/abdulsamadsheikh/Desktop/ProsjektV2/ProsjektV2/KATEGORIER.DTA");
    cout << "\n\nLESER FRA 'KATEGORIER.DTA'\n";
    
    if (!innfil) {
        cout << "NB: Finner ikke filen!" ;
    }

    int antKategorier = 0, antallTing = 0;
    string navn;
    innfil >> antKategorier;
    innfil.ignore();

    for (int i = 0; i < antKategorier; i++) {
        innfil >> antallTing;
        innfil.ignore();
        getline(innfil, navn);
        Kategori* nyKategori = new Kategori(innfil, navn, antallTing);
        gKategori.insert(pair<string, Kategori*>(navn, nyKategori));
    }

    innfil.close();
}

/**
 * En funksjon som lager en ny kategori.
 * @see sjekkKategori()
*/
void Kategorier::nyKategori() {
    string navn;
    cout << "Nytt kategorinavn: ";
    getline(cin, navn);

    string eksisterendeKategori = sjekkKategori(navn);
    if (!eksisterendeKategori.empty()) {
        cout << "Denne kategorien eksisterer allerede" << endl;
        return;
    }

    Kategori* nyKategori = new Kategori(navn);
    gKategori.insert(make_pair(navn, nyKategori));
    cout << "\n\nNy kategori lagt til: " << navn << endl;
}

/**
 * Sjekker om kategori allerede eksisterer eller ikke
*/

string Kategorier::sjekkKategori(string n) {
    string navn = n;
    int antLikeNavn = 0;
    string tempNavn;

    transform(navn.begin(), navn.end(), navn.begin(), ::tolower);

    for (const auto& [kategoriNavn, kategoriPtr] : gKategori) {
        string tempVal = kategoriNavn;
        transform(tempVal.begin(), tempVal.end(), tempVal.begin(), ::tolower);

        if (tempVal == navn) {
            antLikeNavn = 1;
            tempNavn = kategoriNavn;
            break;
        } else if (navn.size() < tempVal.size()) {
            if (tempVal.compare(0, navn.size(), navn) == 0) {
                antLikeNavn++;
                tempNavn = kategoriNavn;
            }
        }
    }

    if (antLikeNavn == 1) {
        return tempNavn;
    } else if (antLikeNavn > 1) {
        cout << "NB: Ikke entydig kategorinavn!";
        return "";
    } else {
        cout << "NB: Kategorien finnes ikke." ;
        return "";
    }
}


/** 
 * En funksjon som skriver ut alle kategorier og alt som er til salgs
 * i de kategoriene.
 * @see Kategori::hentAntallTing
*/
void Kategorier::skrivUtKategorier(){
  cout << "Alle Kategorier:";
  for(const auto & val : gKategori){
    cout << "\n"<<val.first;
    cout << "\t antall ting: " <<(val.second)->hentAntallTing();
  }
}

/**
 * En funksjon som skriver ut en kategori og alt som er til salgs
 * i den Kategorien.
 * @see Kategori::sjekkKategori()
 * @see kategori::skrivData()
*/
void Kategorier::skrivEnKategori(string n){
  string temp = sjekkKategori(n);
  bool finnes=false;
  for(const auto & val : gKategori){
        if(temp == val.first){
          val.second->skrivData();
          finnes=true;
        }
  }
}

/**
 * En funksjon som skriver til filen 'KATEGORIER.DTA'
 * @see Kategori::skrivTilFil 
*/
void Kategorier::skrivTilFil() {
  ofstream utfil("/Users/abdulsamadsheikh/Desktop/ProsjektV2/ProsjektV2/KATEGORIER.DTA");
  cout << "Skriver til filen 'KATEGORIER.DTA'";
  utfil << gKategori.size();
  utfil << "\n";
  for(const auto & val : gKategori){
    (val.second)->skrivTilFil(utfil);
  }
  utfil<<sisteNr;
  utfil.close();
}

/**
 * En funksjon som sletter alt, både kategorier
 * og ting fra fil.
 * @see Kategori::slettAlt
*/
void Kategorier::slettAlt() {
  for (const auto &val : gKategori) {
    val.second->slettAlt();
    delete val.second;
  }
  gKategori.clear();
  cout << "Alle kategorier og ting er sletta fra filene.";
}

/**
 * En funksjon som sletter ting fra fil.
 * @param nr - En tings nummer
 * @see Kategori::slettTing
*/
void Kategorier::slettTing(int nr){
  for(const auto & val : gKategori){
    (val.second)->slettTing(nr);
  }
}
