#ifndef __KATEGORIER_H
#define __KATEGORIER_H

#include <map>
#include <string>
#include <fstream>
#include "kategori.h"

class Kategorier {
private:
    int sisteNr;   //tings siste nr.
    std::map <std::string,Kategori*> gKategori;
public:
    Kategorier();
    ~Kategorier();
    void handling(char k);
    int getTingNr() const;
    void lesFraFil();
    void nyKategori();
    std::string sjekkKategori(std::string n);
    void skrivTilFil();
    void skrivUtKategorier(); ///const????
    void skrivEnKategori(std::string n);   ///const????
    void leggTilKategori() ;
    int hentAntallKategorier() ;
    void slettTing(int nr) ;
    void slettAlt();
    void leggTilSistenr();
    
};

#endif
