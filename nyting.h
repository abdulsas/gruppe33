#ifndef __NYTING_H
#define __NYTING_H

#include <string>
#include <fstream>

class NyTing {
protected:
    int uniktNr,
    kundeNr,
    pris,
    antTilSalgs;
    std::string tingNavn,
    beskrivelse;
public:
    NyTing();
    NyTing(std::ifstream & inn);
    NyTing(int tall);
    NyTing(std::ifstream & inn, int tall);
    ~NyTing();
    virtual int getNummer() const;
    virtual int getKundeNr() const;
    virtual void lesData();
    virtual void skrivData() const;
    virtual void endreData(int valg);
    virtual void skrivTilFil(std::ofstream & ut);
    virtual std::string bruktEllerNy() const;
    virtual int endreAntTilSalgs(int kn);
};

#endif
