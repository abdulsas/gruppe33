#ifndef __KUNDE_H
#define __KUNDE_H
#include <string>
#include <fstream>
#include "enum.h"

class Kunde{
private:
    int KundeNr,
    tlf,
    postNr,
    tingKjopt,
    tingSolgt,
    TingTilSalgs;
    
    std::string navn,
    adresse,
    postSted,
    epost;

public:
    Kunde();
    Kunde(std::ifstream & inn, int tall);
    Kunde(int nr);
    ~Kunde();
    void nullstill();
    void skrivData();
    void skrivDetaljer();
    void lesData();
    void endreData();
    void skrivTilFil(std::ofstream & ut);
    int getKundeNr() const;
    bool slettKunde();
    void plussKjopOgSalg(const kjopOgSalg kOgS) ;

};
#endif
