#ifndef __KATEGORI_H
#define __KATEGORI_H

#include <list>
#include <string>
#include <fstream>
#include "nyting.h"


class Kategori {
  private:
    std::string kategoriNavn;
    std::list <NyTing*> gNyTing;
  public:
    Kategori();
    Kategori(std::string n);
    Kategori(std::ifstream& inn, std::string n, int t);
    ~Kategori();
    int hentSisteNr();
    int hentAntallTing();
    int sjekkKundeNummer(int kn, int tn);
    void lagNyTing();
    //void lesData(); fjernes?
    void endreTing();
    void kjopEnTing(int kn,int tn);
    void skrivData() const;
    void skrivTilFil(std::ofstream & ut);
    void slettTing(int nr);
    void slettAlt();
    std::string hentNavn() ;
};

#endif

