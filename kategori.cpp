/**
 * Funksjonene i klassen 'kategori' og deres definisjoner.
 * @file kategori.cpp
 * @author Abdulsamad Sheikh, Jamal Hussein & Ole Bjørn Halvorsen.
 */
#include <list>
#include <fstream>
#include <string>
#include "kategori.h"
#include "kategorier.h"
#include "nyting.h"
#include "bruktting.h"
#include "LesData3.h"
#include <iostream>
#include "kunde.h"
#include "kunder.h"
#include <algorithm>

using namespace std;

extern Kategorier gKategoribase;
extern Kunder gKundebase;

/**
* Konstruktor
*/
Kategori::Kategori(){
}

/**
* Konstruktor
*/
Kategori::Kategori(string n){
  kategoriNavn=n;
}

/**
* Leser inn egne datamedlemmer fra fil.
*
* @param inn - filen det leses inn datamedlemmer fra.
* @param n   - navnet på kategorien.
*/
Kategori::Kategori(ifstream& inn, string n, int t) {
    kategoriNavn = n;
    int antallTing = t;

    for (int i = 0; i < antallTing; i++) {
        char tegn;
        inn >> tegn;

        switch (tegn) {
            case 'N': {
                NyTing* nyTing = new NyTing(inn);
                gNyTing.push_back(nyTing);
                break;
            }
            case 'B': {
                NyTing* bruktTing = new BruktTing(inn);
                gNyTing.push_back(bruktTing);
                break;
            }
            default:
                cout << "NB: Ukjent tegn i filen!" << endl;
                break;
        }
    }
  }

/**
* Destruktor
*/
Kategori::~Kategori(){
}

/**
* Alle  ting blir skrevet ut, bruker velger hvilken å endre
* og hvilke datamedlem.
*
* @see hentSisteNr()
* @see NyTing::getNummer()
* @see BruktTing::hentSisteNr()
* @see NyTing::endreData(...)
* @see BruktTing::endreData(...)
*/
void Kategori::endreTing() {
    int tingNr, valg;
    skrivData();
    tingNr = lesInt("Endre ting nr", 1, hentSisteNr());

    std::cout << "Hva vil du endre?:" ;
    std::cout << " 1: Navn" ;
    std::cout << " 2: Beskrivelse" ;
    std::cout << " 3: Pris" ;
    std::cout << " 4: Antall" ;
    std::cout << " 5: Alder" ;
    std::cout << " 6: Kvalitet";

    valg = lesInt("Hva vil du endre? ", 0, 6);
    cin.ignore();

    auto it = find_if(gNyTing.begin(), gNyTing.end(),
                      [tingNr](const auto& val) { return val->getNummer() == tingNr; });

    if (it != gNyTing.end()) {
        (*it)->endreData(valg);
    }
}

/**
* Henter antall i kategori.
*
* @return returnerer antall i gNyTing.
*/
int Kategori::hentAntallTing(){
  return gNyTing.size();
}

/**
* Henter sist nummerr fra kategorier
*
* @return returnerer siste nummer brukt.
*/
int Kategori::hentSisteNr(){
  int nummer= gKategoribase.getTingNr();

  return nummer;
}

/**
* Denne funksjonen sjekker om tingen som kunden ønsker å kjøpe er tilgjengelig i den valgte kategorien.
* Deretter vil funksjonen sjekke om kunden prøver å kjøpe sine egne ting.
* Hvis dette er tilfelle, vil funksjonen
* endre relevante variabler både på tingen og kunden som er involvert i handelen.
* @param kn - kundenummer til kjøper
* @param tn - tingens unike nummer
* @see NyTing::getNummer()
* @see BruktTing::hentSisteNummer()
* @see NyTing::endreAntTilSalgs(...)
* @see BruktTing::endreAntTilSalgs(...)
*/
void Kategori::kjopEnTing(int kn, int tn) {
    int tingNr = tn;
    bool slettes = false;

    auto it = find_if(gNyTing.begin(), gNyTing.end(),
                      [tingNr](const auto &val) { return val->getNummer() == tingNr; });

    if (it != gNyTing.end()) {
        slettes = (*it)->endreAntTilSalgs(kn);
    } else {
        cout << "NB: Ikke gyldig nummer i valgt kategori!";
    }

    if (slettes) {
        gNyTing.erase(it);
    }
}

/**
 * @Return Navnet til en kategori
*/
string Kategori::hentNavn(){
  return kategoriNavn;
}

/**
* Brukeren blir spurt om tingen er ny eller brukt, og avhengig av svaret vil enten en brukt eller ny ting bli *opprettet. Dataene til tingen blir så lest inn og lagret i datastrukturen.
*
* @see NyTing::NyTing(...)
* @see BruktTing::BruktTing(...)
* @see NyTing::lesData()
* @see BruktTing::lesData()
* @see hentSisteNr()
* @see Kategorier::leggTilSisteNr()
*/
void Kategori::lagNyTing() {
    char valg;

    do {
        valg = lesChar("B - Brukt\nN - Ny\nQ - Quit? ");
    } while (valg != 'B' && valg != 'N' && valg != 'Q');

    if (valg == 'Q') {
        cout << "\n\tIngen nye ting ble lagt til";
        return;
    }

    NyTing* nyTing = nullptr;

    if (valg == 'N') {
        nyTing = new NyTing(hentSisteNr());
    } else if (valg == 'B') {
        nyTing = new BruktTing(hentSisteNr());
    }

    if (nyTing != nullptr) {
        nyTing->lesData();
        gNyTing.push_back(nyTing);
        gKategoribase.leggTilSistenr();
    }
}

/**
* Sjekker om tingens nummer finnes, og så om medsendt kundenummer
* er selger av tingen.
*
* @param kn - kjøpers kundenummer
* @param tn - tingen som skal kjøpes.
* @see NyTing::getNummer()
* @see BruktTing::getNummer()
* @see NyTing::getKundeNr()
* @return true hvid kjøper == selgers kundenummer.
*         false dersom kunden kan kjøpe tingen.
*/
int Kategori::sjekkKundeNummer(int kn, int tn) {
    for (const auto &val : gNyTing) {
        if (tn == val->getNummer() && kn == val->getKundeNr()) {
            return true;
        }
    }
    return false;
}

/**
* Skriver ut kategoriens navn og antall, og alle tingene i
* kategorien og data, og tilstand.
*
* @see NyTing::bruktEllerNy()
* @see BruktTing::bruktEllerNy()
* @see NyTing::skrivData()
* @see BruktTing::skrivData()
*/
void Kategori::skrivData() const {
  cout << "\n\nKategori: \n" << kategoriNavn << "\nAntall i kategori: " << gNyTing.size();
  
  for (const auto &val : gNyTing) {
    string temp = val->bruktEllerNy();
    cout << "\n" << temp;
    val->skrivData();
    cout << "\n";
  }
}

/**
* Skriver alle klassens datamedlemmer ut til fil.
*
* @param ut - filen datamedlemmene blir skrevet ut til.
* @see gNyTing::bruktEllerNy()
* @see gNyting::skrivTilFil()
*/
void Kategori::skrivTilFil(ofstream & ut){
  ut<<gNyTing.size();
  ut<<" "<<kategoriNavn<<"\n";
  for(const auto & val : gNyTing){
    if(val->bruktEllerNy()=="Ny"){
      ut<<'N'<<" ";
    }else{
     ut<<'B'<<" ";
    }
    val->skrivTilFil(ut);
  }
}

/**
 * Sletter en enkelt ting
 * @param nr - En tings nummer
 * @see gNyting::getKundeNr()
*/
void Kategori::slettTing(int nr) {
    gNyTing.erase(remove_if(gNyTing.begin(), gNyTing.end(),
        [nr](const auto& val) { return val->getKundeNr() == nr; }), gNyTing.end());
}

/**
 * Sletter alle ting innenfor en kategori
*/
void Kategori::slettAlt() {
  while (!gNyTing.empty()) {
    delete gNyTing.front();
    gNyTing.pop_front();
  }
}
