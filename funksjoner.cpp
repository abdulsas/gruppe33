/**
 * Definisjonen av skrivMeny
 * @file funksjoner.cpp
 * @author Abdulsamad Sheikh, Jamal Hussein & Ole Bjørn Halvorsen.
 */

#ifndef __FUNKSJONER_CPP
#define __FUNKSJONER_CPP

#include "funksjoner.h"
#include <iostream>

/**
 * Skriver ut menyen for programmet
*/
void skrivMeny() {
    std::cout << "\nValgmuligheter:\n";
    std::cout << "U N Ny kunde\n";
    std::cout << "U A Skriv alle kunder\n";
    std::cout << "U S <kundenr> Skriv en kunde\n";
    std::cout << "U F <kundenr> Fjern en kunde\n";
    std::cout << "A N Ny kategori\n";
    std::cout << "A A Skriv alle kategorier\n";
    std::cout << "A S <navn> Skriv en navngitt kategori\n";
    std::cout << "T N Ny ting\n";
    std::cout << "T E Endre en ting\n";
    std::cout << "K Kjøp en ting\n";
    std::cout << "Q Avslutt\n";
}

#endif
