#ifndef _BRUKTTING
#define _BRUKTTING

#include "nyting.h"
#include "enum.h"
#include <fstream>
#include <iostream>

class BruktTing : public NyTing{
private:
    int alder;
    Kvalitet gKvalitet;
    
public:
    BruktTing();
    BruktTing(int nr);
    BruktTing(std::ifstream & inn);
    ~BruktTing();
    virtual std::string bruktEllerNy() const;
    virtual int getNummer();
    virtual void lesData();
    virtual void skrivData() const;
    virtual void endreData(int valg);
    virtual void skrivTilFil(std::ofstream & ut);
};

#endif
