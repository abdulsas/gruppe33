#ifndef __CONST_H
#define __CONST_H

const int MINPOSTNUMMER = 100,
          MAXPOSTNUMMER = 9999,
          MINTLF      = 10000000,
          MAXTLF      = 99999999,
          MINALDER  = 0,
          MAXALDER  = 200,
          MINPRIS   = 1,
          MAXPRIS   = 10000,
          MINANT    = 1,
          MAXANT    = 1000,
          MINENUM   = 1,
          MAXENUM   = 5;

#endif
