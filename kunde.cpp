/**
* Funksjonene i klassen 'kunde' og deres definisjoner. 
* @file kunde.cpp
* @author Abdulsamad Sheikh, Jamal Hussein & Ole Bjørn Halvorsen.
*/

#include <iostream>             //  Cout, cin
#include <fstream>              //  ifstream, Ofstream
#include <string>               //  String
#include <list>                  //  List
#include <iomanip>              //  setw, left
#include "kunde.h"
#include "kunder.h"
#include "LesData3.h"
#include "const.h"
using namespace std;

extern Kunder gKundebase;

/**
 * Konstruktør
*/
Kunde::Kunde(){}

/**
 * Opprettelse og initisiering av nytt kundeobjekt
 *
 * @param nr - sisteNr brukt.
*/
Kunde::Kunde(int nr){
    KundeNr=nr+1;
    tlf=postNr=0;
    navn=adresse=postSted=epost="";
    tingKjopt=tingSolgt=TingTilSalgs=0;
}

/**
 * En funksjon som leser klassens datamedlemmer fra fil
 * @param inn - filen som skal leses fra
 * @param tall - siste nr i kundenummeret
*/
Kunde::Kunde(ifstream & inn, int tall){
    KundeNr = tall;
    inn >>TingTilSalgs>>tingSolgt>>tingKjopt>>tlf>>postNr;
    inn.ignore();
    getline(inn,navn);
    getline(inn,adresse);
    getline(inn,postSted);
    getline(inn,epost);
}

/**
 * En funksjon som nullstiller ting til en ny kunde. 
*/
void Kunde::nullstill() {
    tingKjopt = 0;
    tingSolgt = 0;
    TingTilSalgs = 0;
}

/**
 * Destruktør
*/
Kunde::~Kunde(){}

/**
 * En funksjon som skriver ut detaljene til en kunde. 
*/
void Kunde::skrivDetaljer() {
    Kunde::skrivData() ;
    std::cout << "Adresse: " << adresse << ", " << postNr << " " << postSted << std::endl;
    std::cout << "E-post: " << epost << std::endl;
    std::cout << "Antall ting kjopt: " << tingKjopt << std::endl;
    std::cout << "Antall ting solgt: " << tingSolgt << std::endl;
    std::cout << "Antall ting til salgs: " << TingTilSalgs << std::endl;
}

/**
 * Skriver en kundes Kundenummer, navn og tlf
*/
void Kunde::skrivData() {
    std::cout << KundeNr << ": " << navn << " | " << tlf <<std::endl ;
}

/**
 * En funksjon som returnerer en kundes kundenummer
 * @return - Kundenummer 
*/
int Kunde::getKundeNr() const {
    return KundeNr;
}

/**
 * En funksjon som leser inn datamedlemmene til klassen
*/
void Kunde::lesData() {
    std::cout << "Navn: ";
    std::getline(std::cin, navn);
    std::cout << "Adresse: ";
    std::getline(std::cin, adresse);
    std::cout << "Poststed: ";
    std::getline(std::cin, postSted);
    postNr = lesInt("Postnummer", MINPOSTNUMMER, MAXPOSTNUMMER);
    std::cout << "E-post: ";
    std::getline(std::cin, epost);
    tlf = lesInt("Telefonnummer", MINTLF, MAXTLF);
}

/**
 * Endrer antall ting til salgs og kjøpt
 * @param KOgS - Enum for kjøp og salg av ting
*/
void Kunde::plussKjopOgSalg(const kjopOgSalg kOgS) {
    switch (kOgS) {
        case Salg:
            tingSolgt++;
            break;
        case Solgt1:
        case Solgt2:
            tingSolgt++;
            if (kOgS == Solgt2) {
                tingSolgt--;
            }
            break;
        case Kjop:
            tingKjopt++;
            break;
        default:
            break;
    }
}

/**
 * En funksjon som skriver datamedlemmene til klassen til en fil.
 * @param ut - filen det blir skrevet til.
*/
void Kunde::skrivTilFil(ofstream & ut){
    ut<<KundeNr<<" "<<TingTilSalgs<<" "<<tingSolgt<<" "<<tingKjopt<<" "<<tlf<<" "
      <<postNr<<'\n'<<navn<<'\n'<<adresse<<'\n'<<postSted<<'\n'<<epost<<'\n';
}
