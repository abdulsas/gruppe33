#ifndef KUNDER_H
#define KUNDER_H

#include <iostream>
#include "kunde.h"
#include <list>
#include "enum.h"

class Kunder {
public:
    Kunder();
    void leggTilKunde(Kunde* kunde);
    void skrivUtKunder() const;
    void skrivUtEnKunde(int kundeNr) const;
    bool finnKunde(int nr, const kjopOgSalg kOgS);
    void fjernKunde(int kundeNr);
    void opprettNyKunde() ;
    int getSisteNr();
    void handling();
    virtual ~Kunder();
    void lesFraFil();
    void nyKunde() ;
    void skrivTilFil();
    
private:
    std::list <Kunde*> gKunder;
    int sisteNr = 0;
};

#endif
