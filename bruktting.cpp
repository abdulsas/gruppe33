/**
 * Funksjonene i subklassen 'bruktTing' og deres definisjoner.
 * @file bruktting.cpp
 * @author Abdulsamad Sheikh, Jamal Hussein & Ole Bjørn Halvorsen.
 */

#include "bruktting.h"
#include "LesData3.h"
#include <iostream>
#include "enum.h"
#include "const.h"
#include <fstream>
#include "nyting.h"

/**
 * Konstruktør
*/
BruktTing::BruktTing() : NyTing(), alder(0), gKvalitet(SomNy) {}

/**
 * Konstruktør 
*/
BruktTing::BruktTing(int nr){}

/**
 * Konstruktør
*/
BruktTing::BruktTing(std::ifstream & inn) : NyTing(inn), alder(0), gKvalitet(SomNy) {
    int enumNr = 0;
    inn >> enumNr;
    inn >> alder;
    switch (enumNr) {
        case 1:
            gKvalitet = SomNy;
            break;
        case 2:
            gKvalitet = PentBrukt;
            break;
        case 3:
            gKvalitet = Brukt;
            break;
        case 4:
            gKvalitet = GodtBrukt;
            break;
        default:
            gKvalitet = Sliten;
            break;
    }
}

/**
 * Skriver ut data om en brukt ting
 * @see Nyting::skrivData()
*/
void BruktTing::skrivData() const {
    NyTing::skrivData();
    std::cout << " Alder: " << alder << " år" << std::endl;
    std::cout << " Kvalitet: ";
    switch (gKvalitet) {
        case SomNy:
            std::cout << "Som ny";
            break;
        case PentBrukt:
            std::cout << "Pent brukt";
            break;
        case Brukt:
            std::cout << "Brukt";
            break;
        case GodtBrukt:
            std::cout << "Godt brukt";
            break;
        case Sliten:
            std::cout << "Sliten";
            break;
        default:
            std::cout << "Ukjent";
            break;
    }
}

/**
 * Leser inn data om en brukt ting
 * @see Nyting::lesData()
*/ 
void BruktTing::lesData() {
    NyTing::lesData();
    alder = lesInt("Tingens alder (i antall år) ", MINALDER, MAXALDER);
    std::cout << "Velg tingens kvalitet:" ;
    std::cout << "\n 1: Som ny" ;
    std::cout << "\n 2: Pent brukt" ;
    std::cout << "\n 3: Brukt" ;
    std::cout << "\n 4: Godt brukt" ;
    std::cout << "\n 5: Sliten" ;
    int valg = lesInt("\n\nVelg kvalitet ", MINENUM, MAXENUM);
    switch (valg) {
        case 1:
            gKvalitet = SomNy;
            break;
        case 2:
            gKvalitet = PentBrukt;
            break;
        case 3:
            gKvalitet = Brukt;
            break;
        case 4:
            gKvalitet = GodtBrukt;
            break;
        case 5:
            gKvalitet = Sliten;
            break;
        default:
            gKvalitet = SomNy;
            break;
    }
}


/**
 * En funksjon som endrer dataen til en gjenstand 
 * etter ønsket valg fra brukeren
 * @see nyTing::endreData()
 * @param valg - Sender med et valg basert på enum
*/
void BruktTing::endreData(int valg) {
    int enumNr;

    if (valg == 5) {
        alder = lesInt("Tingens nye alder ", MINALDER, MAXALDER);
    }
    else if (valg == 6) {
        std::cout << "Velg tingens nye kvalitet:" ;
        std::cout << " 1: Som ny" ;
        std::cout << " 2: Pent brukt" ;
        std::cout << " 3: Brukt" ;
        std::cout << " 4: Godt brukt" ;
        std::cout << " 5: Sliten" ;
        enumNr = lesInt("Velg kvalitet ", MINENUM, MAXENUM);
        

        switch (enumNr) {
            case 1:
                gKvalitet = SomNy;
                break;
            case 2:
                gKvalitet = PentBrukt;
                break;
            case 3:
                gKvalitet = Brukt;
                break;
            case 4:
                gKvalitet = GodtBrukt;
                break;
            default:
                gKvalitet = Sliten;
                break;
        }
    }

    NyTing::endreData(valg);
}

/**
 * Returnerer en string om tingens tilstand er brukt
*/
std::string BruktTing::bruktEllerNy() const{
  return "Tilstand: Brukt";
}

/**
 * Henter og returnerer en tings unike nummer
*/
int BruktTing::getNummer(){
  return uniktNr;
}

/**
 * En funksjon som skriver alle brukte ting til fil.
 * @see NyTing::skrivTilFil
*/
void BruktTing::skrivTilFil(std::ofstream & ut) {
    NyTing::skrivTilFil(ut);
    switch (gKvalitet) {
        case SomNy:
            ut << 1;
            break;
        case PentBrukt:
            ut << 2;
            break;
        case Brukt:
            ut << 3;
            break;
        case GodtBrukt:
            ut << 4;
            break;
        case Sliten:
            ut << 5;
            break;
    }
    ut << " " << alder << "\n";
}
