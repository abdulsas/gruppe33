/**
*Funksjonene i klassen 'NyTing' og deres definisjoner. 
* @file nyTing.cpp
* @author Abdulsamad Sheikh, Jamal Hussein & Ole Bjørn Halvorsen.
*/
#include <iostream>             //  Cout, cin
#include <fstream>              //  ifstream, Ofstream
#include <string>               //  String
#include <list>                  //  List
#include <iomanip>              //  setw, left
#include "nyting.h"
#include "bruktting.h"
#include "LesData3.h"
#include "const.h"
#include "kunde.h"
#include "kunder.h"
#include "kategorier.h"
using namespace std;

extern Kunder gKundebase;
extern Kategorier gKategoribase;

NyTing::NyTing(std::ifstream & inn){}


/**
*En constructor som oppretter NyTing-objekt
*/
NyTing::NyTing() {
    uniktNr = 0;
    kundeNr = 0;
    pris = 0;
    antTilSalgs = 0;
    tingNavn = "";
    beskrivelse = "";
}

/**
 * Konstruktor
*/
NyTing::NyTing(int tall) {
    lesData();
}

/**
 * Konstruktor for lesing fra fil
 * @param inn - filen som skal leses fra
 * @param tall
*/
NyTing::NyTing(ifstream &inn, int tall) {
    inn >> uniktNr >> kundeNr >> pris >> antTilSalgs;
    inn.ignore();
    getline(inn, tingNavn);
    getline(inn, beskrivelse);
}

/**
 * Nyting-objekt sin destructor
*/
NyTing::~NyTing() {}

/**
 * @return uniktNr - Unike nummeret til tingen
*/
int NyTing::getNummer() const {
    return uniktNr;
}

/**
* @return KundeNr - returnerer kundenummer
*/
int NyTing::getKundeNr() const{
  return kundeNr;
}

/**
*@return string - Ny om tilstanden er ny
*/
string NyTing::bruktEllerNy() const{
    return "Tilstand: Ny";
}

/**
 * En funksjon som leser inn datamedlemmene til klassen
 * @see Kunder::getSisteNr()
*/
void NyTing::lesData() {
    uniktNr=getNummer()+1;
    kundeNr = lesInt("\nSelgers kundenummer ", 0, gKundebase.getSisteNr());
    pris = lesInt("Pris ", MINPRIS, MAXPRIS);
    antTilSalgs = lesInt("Antall til salgs: ", MINANT, MAXANT);
    cout << "Tingens navn: ";
    getline(cin, tingNavn);
    cout << "Beskrivelse: ";
    getline(cin, beskrivelse);
}

/**
 * En funksjon som skriver ut datamedlemmene til klassen
*/
void NyTing::skrivData() const {
    cout << "\nTing nummer: " << uniktNr ;
    cout << "\nNavn: " << tingNavn ;
    cout << "\nPris: " << pris ;
    cout << "\nAntall til salgs: " << antTilSalgs ;
    cout << "\nBeskrivelse: " << beskrivelse ;
}

/**
 * Endrer klassen.
 */
void NyTing::endreData(int valg) {
    switch (valg) {
        case 0:
            cout << "Ingen endringer gjort\n";
            break;
        case 1:
            cout << "Nytt navn: ";
            getline(cin, tingNavn);
            break;
        case 2:
            cout << "Ny beskrivelse: ";
            getline(cin, beskrivelse);
            break;
        case 3:
            pris = lesInt("Ny pris ", MINPRIS, MAXPRIS);
            break;
        case 4:
            antTilSalgs = lesInt("Nytt antall til salgs ", MINANT, MAXANT);
            break;
    }
}

/**
 * En funksjon som skriver datamedlemmene til klassen til en fil.
 * @param ut - filen det blir skrevet til.
*/
void NyTing::skrivTilFil(ofstream & ut){
  cout<<uniktNr;
  ut<<uniktNr<<" "<<kundeNr<<" "<<antTilSalgs<<" "<<pris<<"\n";
  ut<<tingNavn<<"\n";
  ut<<beskrivelse<<"\n";
}

/**
 * @param kn - Antall ting som skal endre
 * Endrer antall ting til salgs 
 * @see kunder::finnKunde()
*/
int NyTing::endreAntTilSalgs(int kn) {
  antTilSalgs--;

  if (antTilSalgs == 0) {
    gKundebase.finnKunde(kundeNr, Solgt2); // Ting blir slettet.
    gKundebase.finnKunde(kn, Kjop);
    return true;
  } else {
    gKundebase.finnKunde(kundeNr, Solgt1); // Ting blir ikke sletta.
    gKundebase.finnKunde(kn, Kjop);
    return false;
  }
}
